function [W,Psi,dPsidz,hW,c,nm] = iow_iwmodes(NN,h,omega,f,m,isRot,isNH)
% IW_MODES computes the vertical structure and speed of an internal wave 
% with frequency omega from the equation W'' + N^2 /c^2 F W = 0. Here, W 
% is the vertical strucuture of the vertical velocity, c the propagation 
% speed, N the buoyancy frequency and F = (1 - rN) / (1 - rf), where 
% rN = (omega/N)^2 and rf = (f/omega^2) with f denoting the rotation rate.
% 
% The returned eigenvectors are checked for physical plausibility. They may
% though still contain numerical modes that have no physical significance.
%
% INPUT ARGUMENTS: 
% NN:               square of buoyancy frequency given at n vertical levels
% h:                grid spacing between vertical levels (n-1 values)
%                   (note that the grid spacing may be variable)
% omega:            frequency of internal wave 
%                   (only used if isRot=1 or isNH=1, see below)
% f:                Coriolis parameter
% m:                return the m largest eigenvalues and eigenvectors
% isRot:            False, if rotation effects are ignored (rf=0)
% is NH:            False, if non-hydrostatic effects are ignored (rN=0)
%
% OUTPUT ARGUMENTS:
% W:                vertial eigenvectors of w, size(n,m)
% Psi:              vertial eigenvectors of u,v,etc, size(n-1,m)
% dPsidz:           derivative of Psi, size(n,m)
% hW:               distance between Psi-levels (at W-levels), size(n,m)
% c:                propagation speeds (vector of size(1,m)
% nm:               number of valid eigenvalues found (smaller than m) 
%
% Info: lars.umlauf@io-warnemuende.de


%% do some consistency checks
n   = length(NN);

if ~isvector(NN) && ~isvector(h)
    error('Input arguments NN and h have to be vectors')
end

if length(NN) ~= length(h)+1
    error('Size of input arguments NN and h inconsistent');
end


%% add effects of rotation
if isRot
    rf = (f/omega)^2;
    if (rf < 0) || (rf > 1)
        error('Omega must be in the range 0...f');
    end
else
    rf = 0;
end


%% add non-hydroststic effects
if isNH
    rN = omega^2 ./ NN;
else
    rN = zeros(size(NN));
end

% compose F
F = (1-rN) ./ (1-rf);


%% compose discritization matrices

% allocate
A    = zeros(n,n);
B    = zeros(n,n);

% this is a simple  trick to yield W(1)=W(n)=0
A(1,1) = 1;
A(n,n) = 1;

% populate A matrix
for i=2:n-1
    a         =  1/( h(i-1) + h(i) );
    b         =  1/h(i-1) + 1/h(i);
    A(i,i-1)  =  2*a/h(i-1);
    A(i,i  )  = -2*a*b;
    A(i,i+1)  =  2*a/h(i);
end

% populate B-matrix
for i=2:n-1
    B(i,i) = F(i)*NN(i);
end


%% compute eigenvales and vectors

[E,L] = eig(A,B);
l     = diag(L)';


%% check and sort in descending order
[W,c,nm] = iw_check_modes(E,l,m);


% compute Eigenfunction for horizontal velocities

Psi          = nan(n-1,nm);
 
for i=1:n-1
     Psi(i,:)  =  ( W(i+1,:) - W(i,:) ) / h(i) ;
end


% compute Eigenfunction for vertical shear

hW               = nan(n,1);   % distance between Psi-levels (at W-levels)
dPsidz           = nan(n,nm);  % shear at W-levels


hW(1)            =  0;         % thicknesses at boundaries undefined
hW(end)          =  0;

dPsidz(1,:)      = 0;          % no shear at boundaries
dPsidz(end,:)    = 0;

for i=2:n-1
    hW(i)        =  0.5*( h(i) + h(i-1) );
    dPsidz(i,:)  =  ( Psi(i,:) - Psi(i-1,:) ) / hW(i) ;
end

end


function [W,c,nm] = iw_check_modes(E,l,m)
% This routine does some plausibility checks, and rejects eigenvalues and 
% eigenvectors that fail.  
%
% INPUT ARGUMENTS: 
% E:                 matrix of size(n,n) with the eigenvectors in columns
% l:                 vector containing corresponding eigenvalues
% m:                 number of desired eigenvectors to be returned
%
% OUTPUT ARGUMENTS: 
% W:                 matrix of size(n,m) with m returned eigenvectors
% c:                 vector containing corresponding phase speeds
%                    (largest speeds come first)
% nm:                number of physically (probably) plausible
%                    eigenvectors


% check if all eigenvalues are real
if sum(~isreal(l)) ~= 0
    error('Complex Eigenvalues detected');
end

% check if all eigenvalues are non-NaN
if sum(isnan(l)) ~= 0
    error('NaN Eigenvalues detected');
end

% remove positive (unphysical) eigenvalues
Ind         = find(l >= 0);
l(  Ind)    = [];
E(:,Ind)    = [];

% sort with smallest eigenvalue first
[l,Ind]     = sort(l,'descend');
E           = E(:,Ind);

% compute phase speeds (largest first)
cRaw        = 1 ./ sqrt(-l);

% return fields
c           = nan(1,m);
W           = nan(size(E,1),m);

nm          = min(m,length(cRaw));
c(1:nm)     = cRaw(1:nm);
W(:,1:nm)   = E(:,1:nm);


end



