% IOW_IW_MODES_TEST 
% This function provides information about how to use the iow_iwmodes() 
% computing internal waves modes in a stratified fluid.
%
% Info: lars.umlauf@io-warnemuende.de



%% set general parameters

isRot      = 0;                % set to 1 to include rotation
isNH       = 0;                % set to 1 to include non-hydrostatic effects

f          = 1.0e-4;           % set Coriolis parameter
omega      = 1.0e-2;           % IW frequency in rad/s (watch out: f<omega<N)

H          = 50;               % water depth
n          = 200;              % number of vertical grid levels
m          = 5;                % number of modes to analyze


%% set grid parameters

GridType   = 'const';           % vertical grid spacing is computed from: 
                                % 'const':  constant layer spacing
                                % 'sine':   sinusoidal layer spacing
                                % 'adapt':  spacing is finer at locations
                                %           with large N
                                
hfac       = 0.1;               % mimum grid size is "hfac" times maximum size                                
                               

%% set stratifiction parameters

StratType  = 'const';           % type of vertical stratification:
                                % 'const':  constant N
                                % 'pycno':  pycnocline a zP with 
                                %           half-width wP


%  buoyancy frequency
NN1       = 1.0e-4;             % used for constant stratification
NNP       = 1.0e-3;             % maximum value in pycnocline

% geometry
zP        = -15;                % z-position of thermocline
wP        = 2;                  % half-width of thermocline                           
                       
%% construct 'measured' stratification

nM   = 1000;                % number of 'measured' data points
NNM  = nan(nM,1);           % buoyancy frequency squared
zM   = linspace(-H,0,nM)';  % vertical positions

switch lower(StratType)
    case 'const'
        NNM = NN1*ones(nM,1);
    case 'pycno'
        % Gaussian bump in NN for thermocline
        NNM = NNP * exp( - 0.5*( (zM-zP)/wP ) .^2  );
    otherwise
        error(['StratType "' StratType '" is unsupported'])
end


%% construct grid

h  = linspace(0,1,n-1)';
         
switch lower(GridType)
    case 'const' 
        h = ones(n-1,1);
    case 'sine' 
        % zooming towards surface and bottom
        h  = linspace(0,1,n-1)';        
        h = sin(pi*h);
        h = max(h,hfac*max(h));  
    otherwise
        error(['GridType "' GridType '" is unsupported'])
end

% stretch to fill complete water column
h  = H/sum(h) * h;

% construct z-axis
z    =  nan(n,1);
z(1) = -H;
for i=2:n
    z(i) = z(i-1)+h(i-1);
end

zp(1)  = -H + 0.5*h(1);

for i=2:n-1
   zp(i) = zp(i-1) + 0.5*( h(i-1) + h(i) );   
end

% interpolated 'measured' values to grid
NN      = interp1(zM,NNM,z);

%  implement no-flux condition at surface and bottom (if applies)
NN(1)   = 0;
NN(end) = 0;


%% plot stratification

figure(1); 
clf; 

plot(NN,z,'ko-')
set(gca,'FontSize',14,'XLim',[0 1.1*max(NN)],'YLim',[z(1) z(n)]);
xlabel('N^2 (s^{-2})');
ylabel('z (m)');
title(['Grid type is: ' GridType '. Stratification type is: ' StratType]);


%% get analytical solution

% pre-allocate
ca  = nan(1,m);    % propgation speed
Wa  = nan(n,m);    % vertical velocity structure of modes 

% compute solution
hasAnlt = true;
switch StratType
    case 'const'
        N = sqrt(NN1);
        for i=1:m
            ca(i)  = N*H/i/pi;
            Wa(:,i)= (-1)^(i+1)*sin(i*pi*z/H);
        end
    otherwise
        warning(['No analytical solution for StratType=' StratType]);
        hasAnlt = false;
end

%% compute numerical solution

% get solution
[W,Psi,dPsidz,hW,c,nm]  = iow_iwmodes(NN,h,omega,f,m,isRot,isNH);


%% plot results


% phase velocities
figure(2)
clf;

plot(c,'ok-')
set(gca,'FontSize',14);
xlabel('Mode');
ylabel('c (m s^{-1})');
title('Phase Speeds');
box on;

% mode structure
figure(3);
clf;

for i=1:nm
    
    h1 = subplot(1,2,1);
    cla(h1);
    
    Plt     = W(:,i);
    PltAnlt = Wa(:,i);
    
    line(Plt    ,z,'Marker','o','Color','k','Parent',h1);
    line(PltAnlt,z,'Marker','o','Color','r','Parent',h1);
    
    if hasAnlt
      legend('numerical','analytical')
    end
  
    set(h1,'FontSize',14,'XLim',[min(Plt) max(Plt)],'YLim',[z(1) z(n)]);
    
    title(['Mode-Number: ' num2str(i)]);
    xlabel('W');
    ylabel('z (m)');
    box on;
    
    
    h2 = subplot(1,2,2);
    cla(h2);
    
    Plt = Psi(:,i);
   
    line( Psi(:,i),zp,'Marker','o','Color','k','Parent',h2);

    set(h2,'FontSize',14,'YTickLabel',{},'XLim',[min(Plt) max(Plt)],'YLim',[z(1) z(n)])
    xlabel('\Psi');
    title(['Press key to proceed.']);    
    box on;  
    
    pause

end
  