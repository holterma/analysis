% illustrates different kinds of Fourier filters
% using the routine iow_fourierfilter


%% define parameters

N          = 20;          % number of sampling intervals
n          = 2;           % mode number of input signal

% filter specification
FilterType =  'bartlett'; % 'box' or 'bartlett'
NDelta     = 8;           % width (points) of filtering window

% other stuff
DoPlot   = 1;             % do plotting of spectra etc.
FontSize = 14;            


%% create some sinusoidal data

% compose signal index vector
% (indexing follows notes with 0 as lowest index)
t        = [0:N-1]';

% generate signal
u       = cos(2*pi*n/N*t);

% compute filtered signal
uFilt   = iow_fourierfilter(u,NDelta,FilterType,DoPlot);


%% plotting

nFig = 0;

% time series
nFig = nFig + 1;
figure(nFig);
clf;

plot(t,u,'o-k',t,uFilt,'o-r')
grid on;

legend('raw','filtered')
set(gca,'FontSize',FontSize)
xlabel('$i$','Interpreter','Latex')
ylabel('$u_i$','Interpreter','Latex')
title('Raw and filtered signals')



