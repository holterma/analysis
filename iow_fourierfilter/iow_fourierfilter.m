function uFilt  =  iow_fourierfilter(u,NDelta,FilterType,DoPlot)
% FOURIERFILTER Filters the input signal u according to a given
% filter window with NDelta points. For argument FilterType, there is
% currently the choice between 'box' and 'bartlett' available. For
% DoPlot=1 some plotting of the signal and filter window (FFT and
% physical space) is done. 
% For filtering, the Fourier coefficients are multiplied by a variable
% transfer function, and the signal is transformed back to physical space. 


%% check input signal
[N,M] = size(u);

if M~=1 
    error('Input signal u is not of size(N,1)')
end

%% compute number of represented frequencies

% index of hightest representable frequency in two-sided spectra
% This depends on the way fft() deals with even and odd record numbers
Nup = N/2;
if round(Nup) == Nup 
   % even   
   Nup   = N/2 + 1;
   m     = [0:Nup-1]'; 
   m     = [-flipud(m) ; m(2:end-1)];
   even  = 1;
else
    % odd
   Nup   = (N+1)/2;
   m     = [0:(Nup-1)]';  
   m     = [-flipud(m) ; m(2:end)];
   even  = 0;
end

%% create filtering window

wD       = 2*pi*m/N*NDelta;

switch lower(FilterType)
    case 'box'
       gHat       = sin(wD/2) ./ (wD/2);
    case 'bartlett'
       gHat       = ( sin(wD/4) ./ (wD/4) ).^2;
    otherwise
        error('unkown FilterType')
end

gHat(Nup)  = 1; 


%% filtering

% everything rescaled by 1/N and shifted to the notation used in the notes

% compute FFT of the signal
uHat     = 1/N*fft(u);
uHat     = fftshift(uHat);

% spectrum is filtered by multiplying with FFT of window function
uFiltHat = uHat .* gHat;

% compute inverse FFT to get the filtered signal in physical space
g        = ifft(N*ifftshift(gHat));
uFilt    = ifft(N*ifftshift(uFiltHat));


%% plotting

if DoPlot

    FontSize = 14;

    nFig = 99;

    % Fourier coefficients
    nFig = nFig + 1;
    figure(nFig);
    clf;

    subplot(4,1,1)
    plot(m,real(uHat),'o-k',m,real(uFiltHat),'o-r')


    grid on;
    set(gca,'FontSize',FontSize,'XTickLabel',{})    
    ylabel('$\hat{u}_m$','Interpreter','latex')  
    title('FFT of signal (real part)');
    legend('raw','filtered')
    
    subplot(4,1,2)
    plot(m,imag(uHat),'o-k',m,imag(uFiltHat),'o-r');

    grid on;
    set(gca,'FontSize',FontSize,'XTickLabel',{})
    ylabel('$\hat{u}_m$','Interpreter','latex')
    title('FFT of signal (imaginary part)')
    legend('raw','filtered')
    
    
    subplot(4,1,3)
    plot(m,gHat,'o-k');

    grid on;
    set(gca,'FontSize',FontSize)
    ylabel('$\hat{g}_m$','Interpreter','latex')
    xlabel('$m$','Interpreter','Latex')
    title('FFT of filter window')

    subplot(4,1,4)
    plot([0:N-1]',g,'o-k');

    grid on;
    set(gca,'FontSize',FontSize)
    ylabel('$g_i$','Interpreter','latex')
    xlabel('$i$','Interpreter','Latex')
    title('Filter window')

end
