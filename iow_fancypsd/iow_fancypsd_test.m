%IOW_FANCYPSD_TEST Test IOW spectral analysis tool
% This routine is supposed to help understanding how iow_fancypsd() works, 
% and what kind of arguments it expects. 
% 
% Info: lars.umlauf@io-warnemuende.de


%% load sample data

% This contains velocities from the Baltic Sea over a couple of months
% The inertial frequency is about f=1.2E-4 (about 0.069 cph).
data = load('data');


% date and time
date   = data.date;          % MATLAB date
days   = (date - date(1));
hours  = days*24;

% velocity components
u      = data.u/100;         % convert to m/s
v      = data.v/100;
w      = u + i*v;            % complex velocity

% sampling parameters
N      = length(date);
dt     = mean(diff(hours));  % sampling period (in hours)
Fs     = 1/dt;               % sampling frequency (in cph)

% some plotting stuff
XULim   = [days(1) days(end)];
ULim    = [-0.15 0.15];

XPLim   = [5e-4 1e0];
PLim    = [2e-6 2e0];


%% do spectral analysis

% detrend
u = detrend(u,'constant');
v = detrend(v,'constant');

% set spectrum parameters
NFFTmax = floor(N/4);       % set largest segment length 
                            % for low-frequency part of the spectrum

Fmin    = 5e-3;             % lowest frequency range from 0 to Fmin
Ffac    = 5;                % increase frequency range by this factor for 
                            % each interation
p       = 0.95;             % set confidence interval
pPlot   = 1e-5;             % plot confidence intervals around this value

% compute u and v spectra
[F,Pu,Ctop,Cbot] = iow_fancypsd(u,NFFTmax,Ffac,Fmin,Fs,p,pPlot);
[F,Pv,Ctop,Cbot] = iow_fancypsd(v,NFFTmax,Ffac,Fmin,Fs,p,pPlot);

% compute rotary spectra (since w is complex this is done automatically
% by iow_fancypsd
[F,Pw,Ctop,Cbot] = iow_fancypsd(w,NFFTmax,Ffac,Fmin,Fs,p,pPlot);




%% plot velocity time series

figure(1);
clf;

subplot(2,1,1)

plot(days,u,'r')

set(gca,'FontSize',14,'XLim',XULim,'YLim',ULim,'XTickLabel',{});
ylabel('u (m s^{-1})');
title('Velocity Timeseries')
box on;

subplot(2,1,2)

plot(days,v,'b')

set(gca,'FontSize',14,'XLim',XULim,'YLim',ULim);
xlabel('t (days)');
ylabel('v (m s^{-1})');
box on;


%% plot spectra

figure(2);
clf;

% first the ordinary spectra of u and v
subplot(2,1,1)

line(F,Pu,'Color','r','LineWidth',2);
line(F,Pv,'Color','b','LineWidth',2);
line(F,Ctop,'Color','k');
line(F,Cbot,'Color','k');

legend('u','v',[num2str(100*p) '%'])
set(gca,'FontSize',14,'XScale','Log','YScale','Log')
set(gca,'XLim',XPLim,'YLim',PLim,'XTickLabel',{})
ylabel('P (m^2 s^{-2}) cph^{-1}');
title('Power Spectral Densities')
box on;


% second the rotary components
subplot(2,1,2)

line(F,imag(Pw),'Color','r','LineWidth',2);
line(F,real(Pw),'Color','b','LineWidth',2);
line(F,Ctop,'Color','k');
line(F,Cbot,'Color','k');

legend('clockwise','anti-clockwise',[num2str(100*p) '%'])
set(gca,'FontSize',14,'XScale','Log','YScale','Log')
set(gca,'XLim',XPLim,'YLim',PLim)
xlabel('f (cph)')
ylabel('P (m^2 s^{-2}) cph^{-1}');
box on;


