function [uAmp,vAmp,alpha,wFit] = iow_fitellipse(t,w,omega)
% IOW_FITELLIPSE Computes elliptical fit and fit parameters from the 
% complex input signal w=u+i*v (corresponding, for example, to two velocity
% components. The theory is described in the section about rotatary 
% spectra in the notes.
%
% Input arguments: 
% t         time vector of size(N,1)
% w         complex signal of size(N,1)
% omega     frequency of fitted siginal (rad/s)
%
% Output arguments:
% uAmp      Major axis amplitude
% vAmp      Minor axis amplitude
% alpha     Rotation angle of major axis with respect to u
% wFit      Fitted signal          


%% get Fourier coefficients and compute parameters

N         = length(t);
wMean     = mean(w);

wHatPlus  = 1/N*  sum(w .* exp(-1i*omega*t) );
wHatMinus = 1/N*  sum(w .* exp( 1i*omega*t) );

APlus     = abs(wHatPlus);
PhiPlus   = angle(wHatPlus);

AMinus    = abs(wHatMinus);
PhiMinus  = angle(wHatMinus);

alpha     = (PhiPlus + PhiMinus)/2;

uAmp      = APlus + AMinus;
vAmp      = APlus - AMinus;


%% reconstruct signal

wFit  =   wHatPlus  * exp(1i*omega*t)   ...
        + wHatMinus * exp(-1i*omega*t) + wMean;


