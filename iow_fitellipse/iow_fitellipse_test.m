% This routine demonstrates how current data (e.g. from tidal flows or 
% near-inertial wave motions can by analyzed with respect to their 
% elliptical character (major axis, rotation).

%% Generate tidal data with some noise
T     = 4;                  % duration of signal
dt    = 0.001;               % time resolution
omega = 2*pi;               % frequency of signal (rad/s)
phi   = pi/4;               % rotation of major axis 
e     = 1.2;                % ellipticty (ratio of major to minor axis)

% construct data
t     = [0:dt:T]';          % time vector  
N     = length(t);
u     = e*cos(omega*t)+(rand(N,1)-0.5)*0.3;   
v     =   sin(omega*t)+(rand(N,1)-0.5)*0.3;

% rotate current ellipse
w     = u+1i*v;
w     = abs(w).*exp(1i*(angle(w)+phi));
u     = real(w);
v     = imag(w);

%% get fit parameters and report

[uAmp,vAmp,alpha,wFit] = iow_fitellipse(t,w,omega);

disp('Fit parameters')
disp(['Ellipticity    = ' num2str(uAmp/vAmp)]);
disp(['Rotation (rad) = ' num2str(alpha)]);
disp(' ')

%% do the plotting

figure(1);
clf;

line(t,real(w),'LineWidth',2,'Color','k')
line(t,real(wFit),'LineWidth',2,'Color','r')

line(t,imag(w),'LineWidth',1,'Color','k')
line(t,imag(wFit),'LineWidth',1,'Color','r')

legend('u','uFit','v','vFit')

set(gca,'FontSize',14)
xlabel('t (s)')
ylabel('u (m/s)')
box on;

figure(2);
clf;

line(real(w),imag(w),'LineWidth',2,'Color','k')
line(real(wFit),imag(wFit),'LineWidth',2,'Color','r')

axis equal;
box on;
legend('w','wFit')

set(gca,'FontSize',14)
xlabel('u (m/s)')
ylabel('v (m/s)')
